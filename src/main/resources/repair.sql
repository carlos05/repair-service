-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-03-2023 a las 00:33:10
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `repair`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cars`
--

CREATE TABLE `cars` (
  `tuition` int(11) NOT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `km` int(11) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `id_client` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id_client` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `cp` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `population` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `telephone2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pieces`
--

CREATE TABLE `pieces` (
  `reference` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pieces`
--

INSERT INTO `pieces` (`reference`, `description`, `price`, `stock`) VALUES
(1, 'llantas', 4094, 100),
(2, 'rines', 1490, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repairs`
--

CREATE TABLE `repairs` (
  `num_repair` int(11) NOT NULL,
  `date_in` datetime DEFAULT NULL,
  `date_out` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `hour` varchar(255) DEFAULT NULL,
  `tuition` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repairs_rapair_detail`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repair_details`
--

CREATE TABLE `repair_details` (
  `id_repair_detail` int(11) NOT NULL,
  `units` int(11) NOT NULL,
  `reference` int(11) DEFAULT NULL,
  `num_repair` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`tuition`),
  ADD KEY `FKpi2nxcsvad7d2y021ak0n5kc9` (`id_client`);

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id_client`);

--
-- Indices de la tabla `pieces`
--
ALTER TABLE `pieces`
  ADD PRIMARY KEY (`reference`);

--
-- Indices de la tabla `repairs`
--
ALTER TABLE `repairs`
  ADD PRIMARY KEY (`num_repair`),
  ADD KEY `FKejde9m20pfwgy7w655bekney6` (`matricula`);

--
-- Indices de la tabla `repairs_rapair_detail`
--
ALTER TABLE `repairs_rapair_detail`
  ADD UNIQUE KEY `UK_de9q4ex7qdsajub6l221wm5r9` (`rapair_detail_id_repair_detail`),
  ADD KEY `FKtropihqonjrxysfp2y1owui4w` (`repairs_num_repair`);

--
-- Indices de la tabla `repair_details`
--
ALTER TABLE `repair_details`
  ADD PRIMARY KEY (`id_repair_detail`),
  ADD KEY `FKi7sxp3mwvqkd0emkc8aj6hmvl` (`reference`),
  ADD KEY `FKobocpppllbrjq7a1upp9ivi9b` (`num_repair`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cars`
--
ALTER TABLE `cars`
  MODIFY `tuition` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pieces`
--
ALTER TABLE `pieces`
  MODIFY `reference` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `repairs`
--
ALTER TABLE `repairs`
  MODIFY `num_repair` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `repair_details`
--
ALTER TABLE `repair_details`
  MODIFY `id_repair_detail` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cars`
--
ALTER TABLE `cars`
  ADD CONSTRAINT `FKpi2nxcsvad7d2y021ak0n5kc9` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id_client`);

--
-- Filtros para la tabla `repairs`
--
ALTER TABLE `repairs`
  ADD CONSTRAINT `FKejde9m20pfwgy7w655bekney6` FOREIGN KEY (`matricula`) REFERENCES `cars` (`tuition`);

--
-- Filtros para la tabla `repairs_rapair_detail`
--
ALTER TABLE `repairs_rapair_detail`
  ADD CONSTRAINT `FK2tm417fkpkaarwgg5qlnmxse0` FOREIGN KEY (`rapair_detail_id_repair_detail`) REFERENCES `repair_details` (`id_repair_detail`),
  ADD CONSTRAINT `FKtropihqonjrxysfp2y1owui4w` FOREIGN KEY (`repairs_num_repair`) REFERENCES `repairs` (`num_repair`);

--
-- Filtros para la tabla `repair_details`
--
ALTER TABLE `repair_details`
  ADD CONSTRAINT `FKi7sxp3mwvqkd0emkc8aj6hmvl` FOREIGN KEY (`reference`) REFERENCES `pieces` (`reference`),
  ADD CONSTRAINT `FKobocpppllbrjq7a1upp9ivi9b` FOREIGN KEY (`num_repair`) REFERENCES `repairs` (`num_repair`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


