package com.example.automotive.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class RqRepair implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int  tuition;
	private String description;
	@NotNull
	@NotEmpty
	@JsonProperty("repair_detail")
	private List<RepairDetail> rapairDetail;
}
