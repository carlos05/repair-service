package com.example.automotive.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Client implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull(message = "The field name cannot be null")
	@NotEmpty(message = "The field name cannot be empty")
	private String name;
	@NotNull(message = "The field name cannot be null")
	@NotEmpty(message = "The field name cannot be null")
	@JsonProperty("last_name")
	private String lastName;
	@NotNull(message = "The field name cannot be null")
	@NotEmpty(message = "El campo option no debe de ser vacio")
	private String address;
	@NotNull(message = "El campo option no debe de ser nulo")
	@NotEmpty(message = "El campo option no debe de ser vacio")
	private String cp;
	private String population;
	private String telephone;
	private String telephone2;

}
