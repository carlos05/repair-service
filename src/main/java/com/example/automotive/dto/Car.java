package com.example.automotive.dto;

import java.io.Serializable;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Car implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull(message = "The field brand cannot be null")
	@NotEmpty(message = "The field brand cannot be empty")
	private String brand;
	@NotNull(message = "The field model cannot be null")
	@NotEmpty(message = "The field model cannot be empty")
	private String model;
	@NotNull(message = "The field color cannot be null")
	@NotEmpty(message = "The field color cannot be empty")
	private String color;
	@NotNull(message = "The field km cannot be null")
	private int km;
	
	@JsonProperty("id_client")
	private  int  idClient;
	
}
