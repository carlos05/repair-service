package com.example.automotive.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.example.automotive.model.RepairDetailEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Repair implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("num_repair")
	private int numRepair;
	private int  matricula;
	private String description;
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@JsonProperty("date_in")
	private Date dateIn;
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@JsonProperty("date_out")
	private Date dateOut;
	private String hour;
	private List<RepairDetailEntity> rapairDetail;
	
}
