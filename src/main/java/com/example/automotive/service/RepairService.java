package com.example.automotive.service;


import java.util.Optional;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.automotive.dto.Car;
import com.example.automotive.dto.Client;
import com.example.automotive.dto.RpBase;
import com.example.automotive.dto.RqRepair;
import com.example.automotive.model.CarEntity;
import com.example.automotive.model.ClientEntity;
import com.example.automotive.model.PieceEntity;
import com.example.automotive.model.RepairEntity;
import com.example.automotive.model.RepairDetailEntity;
import com.example.automotive.repository.ICarRepository;
import com.example.automotive.repository.IClientRepository;
import com.example.automotive.repository.IPieceRepository;
import com.example.automotive.repository.IRepairDetailRepository;
import com.example.automotive.repository.IRepairRepository;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class RepairService implements IRepairService {
	
	/** Injection of dependence clientRepository */
	@Autowired
	private IClientRepository clientRepository;
	
	/** Injection of dependence carRespository */
	@Autowired
	private ICarRepository carRespository;
	
	/** Injection of dependence repairRepository */
	@Autowired
	private IRepairRepository  repairRepository;
	
	/** Injection of dependence repairDetailRepository */
	@Autowired
	private IRepairDetailRepository repairDetailRepository;
	
	/** Injection of	 dependence pieceRepository */
	@Autowired
	private IPieceRepository  pieceRepository;
	
	
	/**
	 * (no-javadoc)
	 * @see com.example.automotive.service.RepairService#createClient(com.example.automotive.model.Client)
	 */
	@Override
	public void createClient(Client client) {
		ClientEntity cl = new ClientEntity();
		cl.setName( client.getName());
		cl.setLastName(client.getLastName());
		cl.setAddress(client.getAddress());
		cl.setCp(client.getCp());
		cl.setPopulation(client.getPopulation());
		cl.setTelephone(client.getTelephone());
		cl.setTelephone2(client.getTelephone2());
		clientRepository.save(cl);
		
	}
	
	/**
	 * (no-javadoc)
	 * @see com.example.automotive.service.RepairService#registerCar(com.example.automotive.model.Car)
	 */
	@Override
	public void registerCar(Car car) {
		CarEntity carEntity= new CarEntity();
		Optional<ClientEntity> client = clientRepository.findById(car.getIdClient());
		carEntity.setBrand(car.getBrand());
		carEntity.setColor(car.getColor());
		carEntity.setKm(car.getKm());
		carEntity.setModel(car.getModel());
		if( client.isPresent()) {
			carEntity.setClient(client.get());
		}
		carRespository.save(carEntity);
		
	}
	
	
	/**
	 * (no-javadoc)
	 * @see com.example.automotive.service.RepairService#createRepair(com.example.automotive.model.RqRepair)
	 */
	@Override
	public RpBase createRepair(RqRepair rqRepair) {
	    RepairEntity repairEntity = new RepairEntity();
	    RpBase response = new RpBase();
	    // to get the car
	    Optional<CarEntity> car = carRespository.findById(rqRepair.getTuition());
	    if( car.isPresent() ) {
	    	repairEntity.setTuition(car.get());
	    }
	    repairEntity.setDescription(rqRepair.getDescription());
        SimpleDateFormat formatterWithTimeZone = new SimpleDateFormat("HH:mm");
		TimeZone timeZone = TimeZone.getTimeZone("America/Mexico_City");
        formatterWithTimeZone.setTimeZone(timeZone);
        String sDate = formatterWithTimeZone.format(new Date());
        repairEntity.setDateIn(new Date());
        repairEntity.setDateOut(new Date());
        repairEntity.setHour(sDate);
        //saving in the RepairEntity
        RepairEntity repairnewEntity =repairRepository.save(repairEntity);
        
       
        rqRepair.getRapairDetail().stream().forEach( x ->{
        	RepairDetailEntity repairDetail = new RepairDetailEntity();
        	String msg="";
        	repairDetail.setRepair(repairnewEntity);
        	repairDetail.setUnits(x.getUnits());
        	Optional<PieceEntity>  pieEntity= pieceRepository.findById(x.getReference());
        	if(pieEntity.isPresent()) {
        		repairDetail.setPiece(pieEntity.get());
        	}
        	//evalue if there is stock in the entity pieces
        	int newStock=pieEntity.get().getStock() - x.getUnits();
        	if( newStock >=0) {
        		pieEntity.get().setStock(newStock);
        		pieceRepository.save(pieEntity.get());
        		repairDetailRepository.save(repairDetail);
        	}else {
        		msg+=response.getMessage()+" The piece "+pieEntity.get().getDescription()+" is not enough, because there is only "+pieEntity.get().getStock()+" pieces";
        		response.setMessage(msg);
        	}        	
        	
        	
        	
        });
        response.setCode("OK");
        return response;
	}
	
	/**
	 * (no-javadoc)
	 * @see com.example.automotive.service.RepairService#getTotalPrice(idRepair)
	 */
	@Override
	public double getTotalPrice(int idRepair) {
		Optional<RepairEntity> repairEntity=repairRepository.findById(idRepair);
		double totalPrice=0.0;
		if( repairEntity.isPresent() ) {
			for(RepairDetailEntity rp: repairEntity.get().getRapairDetail()) {
				
				totalPrice+= rp.getUnits()* rp.getPiece().getPrice();
			}
		}
		return totalPrice;
	}
	
	/**
	 * (no-javadoc)
	 * @see com.example.automotive.service.RepairService#getDetailByIdRepair(idRepair)
	 */
	@Override
	public RepairEntity getDetailByIdRepair(int idRepair) {
		Optional<RepairEntity> repairEntity =repairRepository.findById(idRepair);
		if( repairEntity.isPresent() ) {
			
			return repairEntity.get();
		}
		return null;
		
	}

	
	

}
