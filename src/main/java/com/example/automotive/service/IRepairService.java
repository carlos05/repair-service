package com.example.automotive.service;


import com.example.automotive.dto.Car;
import com.example.automotive.dto.Client;
import com.example.automotive.dto.RpBase;
import com.example.automotive.dto.RqRepair;
import com.example.automotive.model.RepairEntity;



public interface IRepairService {
	/*
	 * Method to create a new client
	 * @param Client data from client
	 * 
	 * */
	public void createClient(Client client);
	
	
	/*
	 * Method to register a  client
	 * @param  Car data from car
	 * 
	 * */
	public void  registerCar(Car car);
	
	/*
	 * Method to create tje bill of repair
	 * @param  RqRepair data to create the bill and detaill of bill
	 * 
	 * */
	public RpBase  createRepair(RqRepair rqRepair);
	
	/*
	 * Method to get total price
	 * @param  idRepair data from car
	 * 
	 * */
	public  double   getTotalPrice(int idRepair);
	
	/*
	 * Method to get the data
	 * @param  idRepair to look up the detail from the repair
	 * 
	 * */
	public  RepairEntity getDetailByIdRepair(int idRepair);
	

}
