package com.example.automotive.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.automotive.dto.Car;
import com.example.automotive.dto.Client;
import com.example.automotive.dto.RpBase;
import com.example.automotive.dto.Status;
import com.example.automotive.model.RepairEntity;
import com.example.automotive.dto.RqRepair;
import com.example.automotive.service.IRepairService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/repair")
public class RepairController {
	
	/* dependence inyection of repair service*/
	@Autowired
	private IRepairService repairService;
	/*
	 * Method to create a new client
	 * 
	 * @param client information
	 * @return ResponseEntity response of the operation
	 * */
	@PostMapping( value="/create-client", produces= MediaType.APPLICATION_JSON_VALUE, consumes= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RpBase> createClient(@Valid @RequestBody Client client){
		RpBase resp = new RpBase();
		HttpStatus http = HttpStatus.CREATED;
		log.info("Startin the creation of client");
		repairService.createClient(client);
		resp.setStatus(new Status());
		resp.setCode("OK");
		resp.setMessage("The client was create");
		log.info("End of creation of client");
		return new ResponseEntity<>(resp, http);
	}
	/*
	 * Method to create a new car
	 * 
	 * @param Car information
	 * @return ResponseEntiry response of the operation
	 * */
	
	@PostMapping( value="/create-car", produces=MediaType.APPLICATION_JSON_VALUE, consumes= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RpBase> createCar(@Valid @RequestBody Car car){
		RpBase resp= new RpBase();
		HttpStatus http= HttpStatus.CREATED;
		log.info("starting the cration of Car");
		repairService.registerCar(car);
		resp.setStatus(new Status());
		resp.setCode("OK");
		resp.setMessage("The car was create");
		log.info("End of creation of client");
		return new ResponseEntity<>(resp, http);
	}
	
	/*
	 * Method to create a bill
	 * 
	 * @param  RqRepair response that contain the information
	 * @return RpRepair return total from all pieces used during the repairation 
	 * 
	 * */
	@PostMapping(value="/create-bill-repair", produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RpBase> createRepair(@Valid @RequestBody RqRepair rqRepair){
		HttpStatus http= HttpStatus.CREATED;
		log.info("startint creation bill repair");
		RpBase rsp=repairService.createRepair(rqRepair);
		log.info("End of creation bill repair");
		return new ResponseEntity<>(rsp, http);
		
	}
	
	/*
	 * Method to create a bill
	 * 
	 * @param  num-repair parameter to get the total of repair of one car
	 * @return RpRepair return total from all pieces used during the repairation 
	 * 
	 * */
	@GetMapping(value="/get-total-bill-repair/{num_repair}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity <RpBase> getTotalBill(@PathVariable("num_repair") int numRepair){
		RpBase resp= new RpBase();
		HttpStatus http= HttpStatus.CREATED;
		log.info("starting to get total repair");
		double total=repairService.getTotalPrice(numRepair);
		log.info("End to get total");
		resp.setCode("OK");
		resp.setMessage("El totla es:"+total);
		return new ResponseEntity<>(resp, http);
	}
	
	/*
	 * Method to get detail of repair
	 * @param idRepair
	 * @return rpDetails
	 * */
	@GetMapping(value="/get-detail-repair/{id_repair}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RepairEntity> getDetailReparation(@PathVariable("id_repair") int idRepair){
		HttpStatus http= HttpStatus.CREATED;
		RepairEntity rpDetail=repairService.getDetailByIdRepair(idRepair);
		return new ResponseEntity<>(rpDetail, http);

	}
	
	
	
	
	
}
