package com.example.automotive.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.automotive.model.RepairDetailEntity;

/*
 * Interface to manage the operations entity RepairDetailEntity
 * 
 * @author JC
 * */
@Repository													   
public interface IRepairDetailRepository extends CrudRepository<RepairDetailEntity,Integer> {
	

}
