package com.example.automotive.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.automotive.model.CarEntity;

/*
 * Interface to manage the operations entity CarEntity
 * 
 * @author JC
 * */
@Repository
public interface ICarRepository extends CrudRepository<CarEntity,Integer> {

}
