package com.example.automotive.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.automotive.model.RepairEntity;

/*
 * Interface to manage the operations entity RepairEntity
 * 
 * @author JC
 * */
@Repository
public interface IRepairRepository extends CrudRepository<RepairEntity, Integer> {

}
