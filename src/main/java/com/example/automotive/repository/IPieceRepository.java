package com.example.automotive.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.automotive.model.PieceEntity;

/*
 * Interface to manage the operations entity PieceEntity
 * 
 * @author JC
 * */
@Repository
public interface IPieceRepository extends CrudRepository<PieceEntity,Integer> {

}
