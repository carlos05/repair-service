package com.example.automotive.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.automotive.model.ClientEntity;
/*
 * Interface to handle the different kind of operation to client entity
 * 
 * @Author JC
 * */

@Repository
public interface IClientRepository extends CrudRepository<ClientEntity,Integer> {

}
