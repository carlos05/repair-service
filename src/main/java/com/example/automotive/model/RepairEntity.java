package com.example.automotive.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity(name="repairs")
public class RepairEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY)
	@Column(name="num_repair")
	private int numRepair;
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@Column(name="date_in")
	private Date dateIn;
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@Column(name="date_out")
	private Date dateOut;
	private String description;
	private String hour;
	@ManyToOne
	@JoinColumn(name="tuition")
	private CarEntity tuition;
	@OneToMany(mappedBy="repair")
	private List<RepairDetailEntity> rapairDetail;
	
}
