package com.example.automotive.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name="cars")
public class CarEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="tuition")
	private int tuition;
	@Column(name="brand")
	private String brand;
	@Column(name="color")
	private String color;
	@Column(name="km")
	private int km;
	@Column(name="model")
	private String model;
	@ManyToOne
    @JoinColumn(name = "id_client")
    private ClientEntity client;
	
}
