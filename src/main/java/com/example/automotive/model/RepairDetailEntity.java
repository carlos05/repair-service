package com.example.automotive.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity(name="repair_details") 
public class RepairDetailEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue( strategy= GenerationType.IDENTITY)
	@Column(name="id_repair_detail")
	private int idRepairDetail;
	private int units;
	@ManyToOne
	@JoinColumn(name="num_repair")
    @JsonIgnoreProperties({"rapairDetail"})
	private RepairEntity repair;
	@ManyToOne
	@JoinColumn(name="reference")
	private PieceEntity  piece;
	
}
